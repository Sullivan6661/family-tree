<?php

namespace App\Models;

use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasSlug;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'gender',
        'parent_id'
    ];

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }

    /**
     * Get Formatted ID
     * 
     * @return string
     */
    public function getFormattedIdAttribute()
    {
        return '#' . $this->id;
    }

    /**
     * Child for this profile.
     *
     * @return HasMany
     */
    public function childs()
    {
        return $this->hasMany(self::class, 'parent_id');
    }

    /**
     * Parent for this profile.
     *
     * @return BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id');
    }

    /**
     * Get Formatted Profile Name
     * 
     * @return string
     */
    public function getFormattedProfileNameAttribute()
    {
        $parent = $this->parent()->first();

        return optional($parent)->name;
    }

    /**
     * Get Formatted Gender
     * 
     * @return string
     */
    public function getFormattedGenderAttribute()
    {
        if (!is_null($this->getAttribute('gender'))){
            if ($this->getAttribute('gender') == 'male'){
                return 'Laki - Laki';
            }

            if ($this->getAttribute('gender') == 'female'){
                return 'Perempuan';
            }
        }
    }

    /**
     * Get Response Attribte
     * 
     * @return array
     */
    public function getResponseAttribute()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'gender' => $this->gender,
            'parent_id' => $this->parent_id
        ];
    }
}
