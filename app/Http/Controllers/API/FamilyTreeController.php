<?php

namespace App\Http\Controllers\API;

use App\Models\Profile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FamilyTreeController extends Controller
{
    /**
     * Show all profiles.
     *
     * @return Response
     */
    public function getData()
    {       
        $profiles = Profile::query()->select(['id', 'name', 'gender', 'slug', 'parent_id']);
        try {
            if (!is_null(request()->childFrom)){
                $who = $profiles->where('slug', request()->childFrom)->first();
                $profiles = $who->childs();
            }
    
            if (!is_null(request()->grandChildFrom)){
                $who = Profile::query()->where('slug', request()->grandChildFrom)->first();
                $childIds = $who->childs()->pluck('id')->toArray();
                $profiles = $profiles->whereIn('parent_id', $childIds);
            }
    
            if (!is_null(request()->auntAndUncleFrom)){
                $who = Profile::query()->where('slug', request()->auntAndUncleFrom)->first();
                $parent = $who->parent()->first();
                $grandParent = $parent->parent()->first();
                $profiles = $profiles->where('parent_id', $grandParent->id)->where('id', '!=', $parent->id);
            }
    
            if (!is_null(request()->cousinFrom)){
                $who = Profile::query()->where('slug', request()->cousinFrom)->first();
                $parent = $who->parent()->first();
                $grandParent = $parent->parent()->first();
                $auntAndUncleIds = Profile::where('parent_id', $grandParent->id)->where('id', '!=', $parent->id)->pluck('id')->toArray();
                $profiles = $profiles->whereIn('parent_id', $auntAndUncleIds);
            }
    
            if (!is_null(request()->gender)){
                if (request()->gender == 'P') {
                    $profiles = $profiles->where('gender', 'female');
                }
    
                if (request()->gender == 'L') {
                    $profiles = $profiles->where('gender', 'male');
                }
            }
    
            $response = $profiles->get();
            if (count($response) < 1) {
                return ResponseFormatter::error($response, 'Data tidak ditemukan', 404);
            }

            $response->transform(function ($profile){
                return $profile->response;
            });
    
            return ResponseFormatter::success($response);

        } catch (\Exception $e) {
            return ResponseFormatter::error(null, 'Terdapat kesalahan dalam memasukan parameter');
        }
    }

}
