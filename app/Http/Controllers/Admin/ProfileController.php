<?php

namespace App\Http\Controllers\Admin;

use App\Models\Profile;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProfileRequest;


class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['table'] = [
            'table_url' => route("profiles.data"),
            'create' => [
                'url' => route("profiles.create"),
                'label' => 'Add Profile',
            ],
            'columns' => [
                [
                    'name' => 'formatted_id',
                    'label' => 'ID',
                ],
                [
                    'name' => 'name',
                    'label' => 'Nama',
                ],
                [
                    'name' => 'formatted_gender',
                    'label' => 'Jenis Kelamin',
                ],
                [
                    'name' => 'formatted_profile_name',
                    'label' => 'Orang Tua',
                ],
                [
                    'name' => 'action',
                    'label' => '#',
                ],
            ]
        ];

        return view('admin.profile.index', $data);
    }

    /**
     * JSON Data for DataTable.
     *
     * @return DataTable
     */
    public function getData()
    {
        $query = Profile::select([
            'id',
            'name',
            'slug',
            'gender',
            'parent_id'
        ]);

        return Datatables::of($query)->addColumn('formatted_id', function($profile){
            return '<strong>' . $profile->formatted_id . '</strong>';
        })->addColumn('formatted_gender', function($item){
            return $item->formatted_gender;
        })->addColumn('formatted_profile_name', function($item){
            return $item->formatted_profile_name;
        })->addColumn('action', function($item){
            $string = '';

            $string .= '<a href="' . route('profiles.edit', $item->id) . '"><button title="Edit" class="btn btn-icon btn-sm btn-success waves-effect waves-light" style="margin-right: 5px;"><i class="fa fa-eye"></i></button></a>';

            $string .= '<button title="Hapus" class="btn btn-icon btn-sm btn-danger waves-effect waves-light delete"><i class="fa fa-trash"></i></button>';
            $string .= '<form action="' . route('profiles.destroy', $item->id) . '" method="POST">' . method_field('delete') . csrf_field() . '</form>';

            return $string;
        })->rawColumns(['formatted_id', 'formatted_gender', 'formatted_profile_name', 'action'])->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['profiles'] = Profile::get(['id', 'name']);

        return view ('admin.profile.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\ProfileRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProfileRequest $request)
    {
        $payload = $this->prepareData($request);
        $profile = Profile::create($payload);

        return redirect()->route('profiles.index')->with('status', 'Profile berhasil dibuat.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect(route('profiles.edit', $id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['object'] = Profile::findOrFail($id);
        $data['profiles'] = Profile::where('id', '!=', $id)->get(['id', 'name']);

        return view('admin.profile.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\ProfileRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProfileRequest $request, $id)
    {
        $payload = $this->prepareData($request);
        $profile = Profile::findOrFail($id);
        $profile->update($payload);

        return redirect()->back()->with('status', 'Profile berhasil diperbarui.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $profile = Profile::findOrFail($id);
        $profile->delete();

        return redirect()->back()->with('status', 'Profile berhasil dihapus.');
    }

    /**
     * Prepare Data.
     *
     * @param array $payload
     *
     * @return array
     */
    public function prepareData($request)
    {
        $payload = $request->only([
            'name',
            'gender',
            'parent_id'
        ]);

        return $payload;
    }

    /**
     * JSON Data for DataTable.
     *
     * @return DataTable
     */
    public function getFamilyTreeData()
    {
        $query = Profile::select([
            'id',
            'name',
            'gender',
            'parent_id'
        ]);

        return Datatables::of($query)->addColumn('pid', function($profile){
            return $profile->parent_id;
        })->rawColumns(['pid'])->make();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function familyTree()
    {
        return view('admin.profile.family-tree');
    }
}
