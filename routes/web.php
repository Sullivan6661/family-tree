<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes(['register' => false]);
Route::get('logout', 'Auth\LoginController@logout');

Route::middleware('admin')->group(function(){
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('/users', 'Admin\UserController');
    Route::get('/users/data/json', 'Admin\UserController@getData')->name('users.data');

    Route::resource('/profiles', 'Admin\ProfileController');
    Route::get('/profiles/data/json', 'Admin\ProfileController@getData')->name('profiles.data');
    Route::get('/family-tree', 'Admin\ProfileController@familyTree')->name('family-tree');
    Route::get('/family-tree/data/json', 'Admin\ProfileController@getFamilyTreeData')->name('family-tree.data');
    
});
