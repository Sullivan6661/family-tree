@extends('layouts.app', [
    'title' => 'Family Tree',
    'breadcrumbs' => [
        'Family Tree'
    ],
])

@section('content')
<section id="family-tree-analytics">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div style="width:100%; height:700px;" id="tree"/>
            </div>
        </div>
    </div>
</section>
@endsection

<style>
    .node.Male rect {
        fill: #2ECC71;
    }

    .node.Female rect {
        fill: #E74C3C;
    }

</style>

@push('after_scripts')
<script src="https://balkan.app/js/OrgChart.js"></script>
<script>
    $.ajax({
        type: "GET",
        url: 'family-tree/data/json',
        data: "check",
        success: function(object){
            nodes = object.data;
            for (var i = 0; i < nodes.length; i++) {
                var node = nodes[i];
                switch (node.gender) {
                    case "female":
                        node.tags = ["Female"];
                        break;
                    case "male":
                        node.tags = ["Male"];
                        break;
                }
            }

            var chart = new OrgChart(document.getElementById("tree"), {
                nodeBinding: {
                    field_0: "name",
                    field_1: "pid",
                    field_2: "gender"
                },
                nodes: nodes
            });
        }
    });


</script>
@endpush