@php
if (isset($object)) {
    $viewData = [
        'title' => 'Edit Profile',
        'breadcrumbs' => [
            'Profile',
            $object->email,
            'Edit',
        ],
    ];
} else {
    $viewData = [
        'title' => 'Add Profile',
        'breadcrumbs' => [
            'Profile',
            'Add',
        ]
    ];
}
@endphp

@extends('layouts.app', $viewData)

@section('content')
{{-- Form Start --}}
@php
if (isset($object)) {
    $actionUrl = route('profiles.update', $object->id);
} else {
    $actionUrl = route('profiles.store');
}
@endphp
<div class="row">
    <div class="{{ isset($object) ? "col-md-12" : "col-md-12" }}">
        <form action="{{ $actionUrl }}" method="POST" enctype="multipart/form-data">

            @if (isset($object))
            {{ method_field('PATCH') }}
            <input type="hidden" name="user_id" value="{{ $object->id }}" />
            @endif
        
            {{ csrf_field() }}
        
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{ $viewData['title'] }}</h4>
                </div>
                <br>
                <div class="card-body">
        
        
                    <div class="form-body">
                        <div class="row">
        
                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-md-2">
                                        <span>Nama</span><span style="color: red;">*</span>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="position-relative has-icon-left">
                                            <input type="text" class="form-control" name="name"
                                                value="{{ isset($object) ? $object->name : old('name') }}" placeholder="Name"
                                                autofocus>
                                            <div class="form-control-position">
                                                <i class="feather icon-user"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
        
                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-md-2">
                                        <span>Jenis Kelamin</span><span style="color: red;">*</span>
                                    </div>
                                    <div class="col-md-10">
                                        <label for="gender">
                                            <input type="radio" name="gender" value="male" {{ isset($object) && $object->gender == 'male' ? 'checked' : ''}}>Laki-Laki
                                            <input type="radio" name="gender" value="female" {{ isset($object) && $object->gender == 'female' ? 'checked' : ''}}>Perempuan
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-md-2">
                                        <span>Parent</span>
                                    </div>
                                    <div class="col-md-10">
                                        <select class="form-control select2" name="parent_id">
                                            <option value>-- Pilih Parent --</option>
                                            @foreach ($profiles as $item)
                                                <option value="{{ $item->id }}" @if (isset($object) && $object->parent_id == $item->id) selected @endif>{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
            
                            <div class="col-md-2"></div>
                            <div class="col-md-10">
                                <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@push('after_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/select/select2.min.css') }}">
@endpush

@push('after_scripts')
<script src="{{ asset('app-assets/vendors/js/forms/select/select2.min.js') }}"></script>
<script>
    $( document).ready(function (){
        $(".select2").select2();
    });  
</script>
@endpush