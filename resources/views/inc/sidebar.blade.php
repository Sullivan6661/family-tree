<ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
    <li class="{{ \Request::is('home') ? 'active' : '' }} nav-item"><a href="{{ route('home') }}"><i class="feather icon-home"></i><span class="menu-title">Dashboard</span></a></li>

    <li class="{{ in_array(\Request::route()->getName(), [
        'users.index',
        'users.create',
        'users.edit',
    ]) ? 'active' : '' }} nav-item"><a href="{{ route('users.index') }}"><i class="feather icon-users"></i><span class="menu-title">Users </span></a></li>

    <li class="{{ in_array(\Request::route()->getName(), [
        'profiles.index',
        'profiles.create',
        'profiles.edit',
    ]) ? 'active' : '' }} nav-item"><a href="{{ route('profiles.index') }}"><i class="feather icon-user"></i><span class="menu-title">Profile </span></a></li>

    <li class="{{ in_array(\Request::route()->getName(), [
        'family-tree',
    ]) ? 'active' : '' }} nav-item"><a href="{{ route('family-tree') }}"><i class="feather icon-book-open"></i><span class="menu-title">Family Tree </span></a></li>


</ul>
